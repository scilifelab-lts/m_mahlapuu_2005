# Gene Set Collections: Homo sapiens (Human)

The gene set collection (`.gmt`) files in this directory were retrieved from the [Molecular Signatures Database](https://www.gsea-msigdb.org/gsea/msigdb/collections.jsp) (v7.2), except for those derived from [Human-GEM](https://github.com/SysBioChalmers/Human-GEM), which were obtained from a [Gene Set Analysis package on GitHub](https://github.com/JonathanRob/GeneSetAnalysisMatlab/tree/master/gsc).



