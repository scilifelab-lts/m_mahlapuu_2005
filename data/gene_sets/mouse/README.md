# Gene Set Collections: Mus musculus (Mouse)

The gene set collection files in this directory were retrieved from http://bioinf.wehi.edu.au/MSigDB/v7.1/. See the [associated website](http://bioinf.wehi.edu.au/MSigDB/) for additional details.

