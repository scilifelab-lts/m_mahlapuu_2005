%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  GEM-based enrichment analysis with
%  subsystem and reporter metabolite
%  gene sets extracted from Human-GEM 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% dependency
% this script depends on Human-GEM v1.10 and Gene Set Analysis Matlab (GSAM)

% load Human-GEM v1.10.0
load('Human-GEM.mat')

% replace Ensembl gene ids with UniProt ids
[grRules_new,genes,rxnGeneMat] = translateGrRules(ihuman.grRules,'UniProt');
ihuman.grRules = grRules_new;
ihuman.genes = genes;
ihuman.rxnGeneMat = rxnGeneMat;


% load DE results of SMC proteomics data
DEdata = readtable('../../data/proteomics/proteomics_cell_lines/total_peptides_processed.csv');
proteinIDs = DEdata.Accession;                         % gene IDs
log2FC     = DEdata.log2fc_treated_control_vs_stk25;   % log2(fold-changes)
pvals      = DEdata.pval_treated_control_vs_stk25;     % fold-change significance (p-values)

% remove empty values by re-index the cells
ind2del = union(find(isnan(log2FC)),find(isnan(pvals)));
newInd = transpose(setdiff([1:length(proteinIDs)],ind2del));
proteinIDs = proteinIDs(newInd);        % Uniprot IDs
log2FC     = log2FC(newInd);            % log2(fold-changes)
pvals      = pvals(newInd);             % fold-change significance (p-values)

%% reporter metabolite

% Extract a metabolite-based GSC from the Human-GEM
gsc_met = extractMetaboliteGSC(ihuman, false);

% gene set analysis
GSAresReMet = geneSetAnalysis(proteinIDs, pvals, log2FC, gsc_met, ...
                       'method', 'Reporter', 'nPerm', 50000, 'gsSizeLim', [20, 200]);
GSAresReMet.Properties.Description = 'Reporter Metabolite';

% clear mixed-directional classes
GSAresReMet = removevars(GSAresReMet,[6 7 8 9 10 11]);

% plot reporter metabolites
GSAheatmap(GSAresReMet, 'filterMethod', 'pval', 'filterThreshold', 0.025, 'colorMax', 5);
set(gca,'FontName','Arial','FontSize',14);
xtickangle(45)
title(GSAresReMet.Properties.Description,'FontSize',16,'FontWeight','bold')



%% subsystem

% Extract a subsystem-based gene set
% note that there is one reaction (index: 8423) has two subsystem names
ihuman.subSystems{8423} = 'Heme synthesis';
gsc_subSys = extractSubsystemGSC(ihuman);

% gene set analysis
GSAresSubsys = geneSetAnalysis(proteinIDs, pvals, log2FC, gsc_subSys, ...
                    'method', 'Reporter', 'nPerm', 50000, 'gsSizeLim', [5, 300]);
GSAresSubsys.Properties.Description = 'Subsystem'; 

% clear mixed-directional classes
GSAresSubsys = removevars(GSAresSubsys,[6 7 8 9 10 11]);

% plot subsystem
GSAheatmap(GSAresSubsys, 'filterMethod', 'pval', 'filterThreshold', 0.05, 'colorMax', 5);
set(gca,'FontName','Arial','FontSize',14);
xtickangle(45)
title(GSAresSubsys.Properties.Description,'FontSize',16,'FontWeight','bold')


